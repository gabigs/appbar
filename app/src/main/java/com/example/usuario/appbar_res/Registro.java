package com.example.usuario.appbar_res;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class Registro extends AppCompatActivity {

    EditText etur, etcr, eter;
    Button btncr, btnpr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        //Codigo flecha de retorno
        setupActionBar();
        //Termina

        //Validacion de campos
        etur = (EditText) findViewById(R.id.etur);
        etcr = (EditText) findViewById(R.id.etcr);
        eter = (EditText) findViewById(R.id.eter);
        btncr = (Button) findViewById(R.id.btncr);
        btnpr = (Button) findViewById(R.id.btnpr);
        //Termina
    }

    //Flecha de retorno
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Registro");
        }
    }
    //Termina
}
