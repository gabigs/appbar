package com.example.usuario.appbar_res;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class InicioSesion extends AppCompatActivity {

    EditText etu, etc;
    Button btnc, btnp;

    private FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_sesion);
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        listener =  new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user =mAuth.getCurrentUser();
                if (user != null){

                }else{

                }

            }
        };


        //Codigo para la flecha de retorno
        setupActionBar();
        //Termina

        //Validacion de campos
        etu = (EditText) findViewById(R.id.etu);
        etc = (EditText) findViewById(R.id.etc);
        btnc = (Button) findViewById(R.id.btnc);
        btnp = (Button) findViewById(R.id.btnp);
        //Termina

        btnc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String usuario = etu.getText().toString();
                String contrasena = etc.getText().toString();

                if(!usuario.isEmpty() && !contrasena.isEmpty()){
                    mAuth.signInWithEmailAndPassword(usuario, contrasena).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                Intent intent = new Intent(getApplicationContext(), Menu.class);
                                startActivity(intent);
                            }else {
                                Toast.makeText(getApplicationContext(), "Usuario incorrecto", Toast.LENGTH_LONG).show();
                            }

                        }
                    });
                }else {
                    Toast.makeText(getApplicationContext(), "Completa los campos", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(listener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(listener);
    }

    //Flecha de retorno
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Inicio Sesión");
        }
    }
    //Termina
}
