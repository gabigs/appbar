package com.example.usuario.appbar_res;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class Reservaciones extends AppCompatActivity {

    EditText eturs;
    Button btnars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservaciones);

        //Flecha retorno
        setupActionBar();
        //Termina

        //Validacion de campos
        eturs = (EditText) findViewById(R.id.eturs);
        btnars = (Button) findViewById(R.id.btnars);
        //Termina

    }

    //Flecha de retorno
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Reservación");
        }
    }
    //Termina
}
